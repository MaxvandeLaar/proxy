let Urls = function(env){
    env = env? `-${env}`: '';
    let baseUrl = `https://api${env}.example.com`;
    let users = 'users';
    let employees = 'employees';
    let authorization = 'authorization';
    let teams = 'teams';
    let locations = 'locations';

    return {
        users: {
            all: `${baseUrl}/${users}/data.xml`,
            byId: `${baseUrl}/${users}`
        },
        employees: {
            all: `${baseUrl}/${employees}/data.xml`,
            byId: `${baseUrl}/${employees}`
        },
        authorization: {
            profiles: {
                all: `${baseUrl}/${authorization}/profiles`,
                byId: `${baseUrl}/${authorization}/profiles`
            }
        },
        teams: {
            all: `${baseUrl}/${teams}/data.xml`,
            byId: `${baseUrl}/${teams}`
        },
        locations: {
            all: `${baseUrl}/${locations}`,
            byId: `${baseUrl}/${locations}`

        }
    };
};

module.exports = Urls;