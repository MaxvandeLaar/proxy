let request = require('request');
let xml2js = require('xml2js');
let Promise = require('promise');

let Employees = function (opts, urls) {
    Employees.options = opts;
    Employees.urls = urls;
};

Employees.prototype.getAll = function () {
    return new Promise((fulfill, reject) => {
        Employees.options.url = Employees.urls.all;

        let xmlParser = new xml2js.Parser({explicitArray: false, explicitRoot: false});
        request.get(Employees.options, function (error, response, body) {
            if (!error) {
                xmlParser.parseString(body, function (err, data) {
                    if (!err) {
                        fulfill({employees: data.employee});
                    } else {
                        reject(err);
                    }
                });
            } else {
                reject(error);
            }
        });
    });
};

Employees.prototype.getById = function (id) {
    return new Promise((fulfill, reject) => {
        Employees.options.url = `${Employees.urls.byId}/${id}`;
        request.get(Employees.options, function (error, response, body) {
            if (!error) {
                let jsonResult = null;
                try {
                    jsonResult = JSON.parse(body);
                } catch (e) {
                    reject(e);
                }
                fulfill(jsonResult);
            } else {
                reject(error);
            }
        });
    });
};

module.exports = Employees;