let request = require('request');
let Promise = require('promise');

let Locations = function (opts, urls) {
    Locations.options = opts;
    Locations.urls = urls;
};

Locations.prototype.getAll = function () {
    return new Promise((fulfill, reject) => {
        Locations.options.url = Locations.urls.all;
        request.get(Locations.options, function (error, response, body) {
            if (!error) {
                let jsonResult = null;
                try {
                    jsonResult = JSON.parse(body);
                } catch (e) {
                    reject(e);
                }
                fulfill(jsonResult);
            } else {
                reject(error);
            }
        });
    });
};

Locations.prototype.getById = function (id) {
    return new Promise((fulfill, reject) => {
        Locations.options.url = `${Locations.urls.byId}/${id}`;
        request.get(Locations.options, function (error, response, body) {
            if (!error) {
                let jsonResult = null;
                try {
                    jsonResult = JSON.parse(body);
                } catch (e) {
                    reject(e);
                }
                fulfill(jsonResult);
            } else {
                reject(error);
            }
        });
    });
};

module.exports = Locations;