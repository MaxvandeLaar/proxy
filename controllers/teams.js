let request = require('request');
let xml2js = require('xml2js');
let Promise = require('promise');

let Teams = function (opts, urls) {
    Teams.options = opts;
    Teams.urls = urls;
};

Teams.prototype.getAll = function () {
    return new Promise((fulfill, reject) => {
        Teams.options.url = Teams.urls.all;

        let xmlParser = new xml2js.Parser({explicitArray: false, explicitRoot: false});
        request.get(Teams.options, function (error, response, body) {
            if (!error) {
                xmlParser.parseString(body, function (err, data) {
                    if (!err) {
                        fulfill({teams: data.team});
                    } else {
                        reject(err);
                    }
                });
            } else {
                reject(error);
            }
        });
    });
};

Teams.prototype.getById = function (id) {
    return new Promise((fulfill, reject) => {
        Teams.options.url = `${Teams.urls.byId}/${id}`;
        request.get(Teams.options, function (error, response, body) {
            if (!error) {
                let jsonResult = null;
                try {
                    jsonResult = JSON.parse(body);
                } catch (e) {
                    reject(e);
                }
                fulfill(jsonResult);
            } else {
                reject(error);
            }
        });
    });
};

module.exports = Teams;