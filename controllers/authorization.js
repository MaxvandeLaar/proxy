let request = require('request');
let Promise = require('promise');

let Authorization = function (opts, urls) {
    Authorization.options = opts;
    Authorization.urls = urls;
};

Authorization.prototype.profiles = {};

Authorization.prototype.profiles.getAll = function () {
    return new Promise((fulfill, reject) => {
        Authorization.options.url = Authorization.urls.profiles.all;

        request.get(Authorization.options, function (error, response, body) {
            if (!error) {
                let jsonResult = null;
                try {
                    jsonResult = JSON.parse(body);
                } catch (e) {
                    console.error(e);
                    reject(body);
                }
                fulfill(jsonResult);
            } else {
                reject(error);
            }
        });
    });
};

Authorization.prototype.profiles.getById = function (id) {
    return new Promise((fulfill, reject) => {
        Authorization.options.url = `${Authorization.urls.profiles.byId}/${id}`;
        request.get(Authorization.options, function (error, response, body) {
            if (!error) {
                let jsonResult = null;
                try {
                    jsonResult = JSON.parse(body);
                } catch (e) {
                    reject(e);
                }
                fulfill(jsonResult);
            } else {
                reject(error);
            }
        });
    });
};

module.exports = Authorization;