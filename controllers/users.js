let request = require('request');
let xml2js = require('xml2js');
let Promise = require('promise');

let Users = function (opts, urls) {
    Users.options = opts;
    Users.urls = urls;
};

Users.prototype.getAll = function () {
    return new Promise((fulfill, reject) => {
        Users.options.url = Users.urls.all;

        let xmlParser = new xml2js.Parser({explicitArray: false, explicitRoot: false});
        request.get(Users.options, function (error, response, body) {
            if (!error) {
                xmlParser.parseString(body, function (err, data) {
                    if (!err) {
                        fulfill({users: data.user});
                    } else {
                        reject(err);
                    }
                });
            } else {
                reject(error);
            }
        });
    });
};

Users.prototype.getById = function (id) {
    return new Promise((fulfill, reject) => {
        Users.options.url = `${Users.urls.byId}/${id}`;
        request.get(Users.options, function (error, response, body) {
            if (!error) {
                let jsonResult = null;
                try {
                    jsonResult = JSON.parse(body);
                } catch (e) {
                    reject(e);
                }
                fulfill(jsonResult);
            } else {
                reject(error);
            }
        });
    });
};

module.exports = Users;