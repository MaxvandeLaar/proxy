let fs = require('fs');
let path = require('path');
let Urls = require(path.resolve(__dirname, 'configs/urls'));
let Users = require(path.resolve(__dirname, 'controllers/users'));
let Employees = require(path.resolve(__dirname, 'controllers/employees'));
let Authorization = require(path.resolve(__dirname, 'controllers/authorization'));
let Teams = require(path.resolve(__dirname, 'controllers/teams'));
let Locations = require(path.resolve(__dirname, 'controllers/locations'));

let Proxy = function(certFile, keyFile, passphrase, environment = '') {
    Proxy.requestOptions = {
        cert: fs.readFileSync(certFile),
        key: fs.readFileSync(keyFile),
        passphrase: passphrase,
        headers: {
            'accept': 'application/json'
        }
    };
    Proxy.urls = new Urls(environment);
    Proxy.prototype.users = new Users(Proxy.requestOptions, Proxy.urls.users);
    Proxy.prototype.employees = new Employees(Proxy.requestOptions, Proxy.urls.employees);
    Proxy.prototype.authorization = new Authorization(Proxy.requestOptions, Proxy.urls.authorization);
    Proxy.prototype.teams = new Teams(Proxy.requestOptions, Proxy.urls.teams);
    Proxy.prototype.locations = new Locations(Proxy.requestOptions, Proxy.urls.locations);
};

Proxy.prototype.listUsedUrls = function(){
    return Proxy.urls;
};

module.exports = Proxy;